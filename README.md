# Warren -- Crypto -- TUCTF2019

[Source](https://gitlab.com/tuctf2019/crypto/warren)

## Chal Info

Desc: `A true cipher buffet lies ahead.`

Flag: `TUCTF{th4nks_f0r_d1n1ng_4641n_4t_th3_W4rr3n_buff3t}`

## Deployment

[Docker Hub](https://hub.docker.com/r/asciioverflow/warren)

Ports: 8888

Example usage:

```bash
docker run -d -p 127.0.0.1:8888:8888 asciioverflow/warren:tuctf2019
```
