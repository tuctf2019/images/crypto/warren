
FROM python:3

WORKDIR /usr/src/app

RUN apt -y update
RUN apt -y install socat

COPY ./src .

RUN chmod +x /usr/src/app/chal.py

CMD socat TCP-LISTEN:8888,fork,reuseaddr EXEC:"/usr/src/app/chal.py"
